import dracula.draw

config.load_autoconfig()

config.bind("<f12>", "inspector")
config.unbind("<ctrl+tab>")
config.bind("<ctrl+tab>", "tab-next")
config.bind("<ctrl+shift+tab>", "tab-prev")

# Set search engines
config.set(
    "url.searchengines",
    {
        "DEFAULT": "https://anon.sx/search?q={}",
        "!g": "https://www.google.com/search?q={}",
        "!r": "https://reddit.com/r/{}",
        "!rs": "https://reddit.com/search?q={}",
        "!yt": "https://www.youtube.com/results?search_query={}",
        "!wiki": "https://en.wikipedia.org/wiki/Search?search={}",
    },
)
config.set("editor.command", ["wezterm", "start", "--", "nvim", "{file}"])

# Url settings
config.set("url.default_page", "https://anon.sx")
config.set("url.start_pages", [config.get("url.default_page")])

dracula.draw.blood(c, {"spacing": {"vertical": 6, "horizontal": 8}})
